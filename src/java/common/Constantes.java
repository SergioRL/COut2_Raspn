/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package common;

/**
 *
 * @author SONY
 */

public class Constantes {
    
    //VARIABLE FICHERO CONFIG
    public static final String NOMBRE_FICHERO = "COut2_json_restful.txt";
    
    //PATHS
    public static final String GET_ESTADO_SISTEMA = "/getEstadoActual";
    
    public static final String GET_ENCENDER_AUTOMATICO = "/activarAutomatico";
    
    public static final String GET_APAGAR_AUTOMATICO = "/desactivarAutomatico";
    
    public static final String GET_ABRIR_ACTUADOR = "/activarActuador";
    
    public static final String GET_CERRAR_ACTUADOR = "/desactivarActuador";
    
    public static final String GET_NIVEL_CO2_ACTUAL = "/getMuestra";
    
    public static final String GET_GET_NIVEL_MAX_CO2 = "/getNivelMax"; 
    
    public static final String POST_SET_NIVEL_MAX_CO2 = "/setNivelMax";
    
    //KEYS JSON MUESTRAS
    public static final String JSON_MUESTRA_NIVEL_CO2 = "1.1";
    
    //KEYS JSON
    public static final String JSON_ESTADO_AUTOMATICO = "2";
    public static final String JSON_ESTADO_ACTUADOR = "3";
    public static final String JSON_MAX_NIVEL_CO2 = "4";
    
}
