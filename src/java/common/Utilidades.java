/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package common;

import javax.faces.context.FacesContext;

/**
 *
 * @author SONY
 */
public class Utilidades {
    
    public static boolean estaVacio(String s){
        return s == null || s.trim().length() == 0;
    }
    
    public static Object buscarBean(String nombreBean){
        try{
            return FacesContext.getCurrentInstance().getApplication().getELResolver().getValue(FacesContext.getCurrentInstance().getELContext(), null, nombreBean);            
        }catch(Exception e){
            e.printStackTrace();
            return null;
        }
    }
    
    public static String getExtension(String file) {
        String[] archivoSplit = file.split("\\.");
        return archivoSplit[archivoSplit.length - 1];
    }
    
}
