/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package arduinoCom;

import java.util.logging.Level;
import java.util.logging.Logger;
import jssc.SerialPort;
import jssc.SerialPortException;
import jssc.SerialPortList;

/**
 *
 * @author SeRRi
 */
public class ArduinoSerial {
    private static SerialPort serialPort;
    
    public ArduinoSerial(){}
    
    public static SerialPort getSerialPort() throws SerialPortException{
        
        if(serialPort == null)
            initConexion();
            
        
        return serialPort;
    }
    
    private static void initConexion() throws SerialPortException{
        String[] puertosDisponibles = SerialPortList.getPortNames();
         
        if(puertosDisponibles.length < 1){
            throw new ArduinoComException("No hay puertos disponibles.");
        }else{
            int i = 0;
            boolean creado = false;
            while(i<puertosDisponibles.length && !creado){
                if(puertosDisponibles[i].matches(".*/ttyS.")){
                    serialPort = new SerialPort(puertosDisponibles[i]);
                    creado = true;
                }
                i++;
            }
        }
        
        synchronized(serialPort){
            serialPort.openPort();
            serialPort.setParams(SerialPort.BAUDRATE_38400,
                                 SerialPort.DATABITS_8,
                                 SerialPort.STOPBITS_1,
                                 SerialPort.PARITY_NONE);
        }
    }
    
    private static synchronized String mandarComando(String operacion, String[] argumentos){
        String comando = operacion;
        String result = null;
        
        if(comando != null){
            if(argumentos != null){
                for(int i = 0; i < argumentos.length ; i++){
                    if(argumentos[i] != null){
                        comando += ' '+argumentos[i];
                    }
                }
            }
            comando += ';';
            
            byte[] inBuffer = null;
            
            try{
                serialPort = getSerialPort();

                synchronized(serialPort){
                    try {
                        byte[] auxBuff;
                        String auxResult = "";
                        serialPort.purgePort(SerialPort.PURGE_TXCLEAR);
                        long sendMillis = System.currentTimeMillis();
                        serialPort.purgePort(SerialPort.PURGE_RXCLEAR);
                        inBuffer = serialPort.readBytes();
                        serialPort.writeBytes(comando.getBytes());
                        while ((result == null || serialPort.getInputBufferBytesCount() > 0) && System.currentTimeMillis()-sendMillis < 5000) {
                            if(serialPort.getInputBufferBytesCount() > 0){
                                auxBuff = serialPort.readBytes();
                                auxResult += new String(auxBuff);
                            }
                        }
                        inBuffer = auxResult.getBytes();
                    } catch (SerialPortException ex) {
                        Logger.getLogger(ArduinoSerial.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
                if(inBuffer == null){
                    Logger.getLogger(ArduinoSerial.class.getName()).log(Level.SEVERE, null,new ArduinoComException("No hubo respuesta desde Arduino."));
                }else{
                    result = new String(inBuffer);
                    if("".equals(result))
                        result = "ERROR: Error respuesta vacía.";
                }
            }catch(SerialPortException ex){
                Logger.getLogger(ArduinoSerial.class.getName()).log(Level.SEVERE, "ERROR: Error al creal el SerialPort");
                result = null;
            }
        }
        return result;
    }
    
    //FUNCIONES DISPONIBLES
    
    public static String encenderAutomático(){
        return mandarComando(Comandos.OP_AUTO_ON, null);
    }
    
    public static String apagarAutomático(){
        return mandarComando(Comandos.OP_AUTO_OFF, null);
    }
    
    public static String abrirActuador(){
        return mandarComando(Comandos.OP_OPEN_ACTUATOR, null);
    }
    
    public static String cerrarActuador(){
        return mandarComando(Comandos.OP_CLOSE_ACTUATOR, null);
    }
    
    public static String getEstadoActual(){
        return mandarComando(Comandos.OP_GET_STATE, null);
    }
    
    public static String getNivelCO2Actual(){
        return mandarComando(Comandos.OP_GET_CO2_LEVEL, null);
    }
    
    public static String getNivelMaximoCO2(){
        return mandarComando(Comandos.OP_GET_MAX_CO2_LEVEL, null);
    }
    
    public static String setNivelMaximoCO2(double nivelMax){
        String[] argumentos = new String[1];
        
        argumentos[0] = String.format("%.2f", nivelMax);
        
        return mandarComando(Comandos.OP_SET_MAX_CO2_LEVEL, argumentos);
    }
}
