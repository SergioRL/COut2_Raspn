/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package arduinoCom;

/**
 *
 * @author SeRRi
 */
public class ArduinoComException extends RuntimeException{

    public ArduinoComException() {
        super();
    }
    
    public ArduinoComException(String mensaje){
        super(mensaje);
    }
}
