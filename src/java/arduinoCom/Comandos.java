/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package arduinoCom;

/**
 *
 * @author SeRRi
 */
public class Comandos {
    public static final String OP_AUTO_ON = "AUON";
    public static final String OP_AUTO_OFF = "AUOFF";
    public static final String OP_OPEN_ACTUATOR = "OPACT";
    public static final String OP_CLOSE_ACTUATOR = "CLACT";
    public static final String OP_GET_STATE = "GETST";
    public static final String OP_GET_CO2_LEVEL = "GETLVL";
    public static final String OP_GET_MAX_CO2_LEVEL = "GETMAXLVL"; 
    public static final String OP_SET_MAX_CO2_LEVEL = "SETMAXLVL"; 
}
