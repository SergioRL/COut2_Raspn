/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package restfulWS;

import java.util.Timer;
import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import restfulWS.task.ImprimirHostTask;
import restfulWS.task.NivelMaxCO2Task;

/**
 *
 * @author SeRRi
 */

@Singleton
@Startup
public class TaskManager {
    private static final int PERIODO_MAX_CO2_TASK = 45000;
    private static final int PERIODO_PRINT_HOST_TASK = 120000;
    
    private static Timer timerMaxCO2, timerPrintHost;
    
    @PostConstruct
    public void init() {
        if(timerMaxCO2 == null){
            timerMaxCO2 = new Timer();
            timerMaxCO2.scheduleAtFixedRate(new NivelMaxCO2Task(), 5000, PERIODO_MAX_CO2_TASK);
        }

        if(timerPrintHost == null){
            timerPrintHost = new Timer();
            timerPrintHost.scheduleAtFixedRate(new ImprimirHostTask(), 5000, PERIODO_PRINT_HOST_TASK);
        }
    }

    @PreDestroy
    public void destroy() {
      if(timerMaxCO2 != null){
            timerMaxCO2.cancel();
        }

        if(timerPrintHost != null){
            timerPrintHost.cancel();
        }
    }
}
