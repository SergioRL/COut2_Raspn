/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package restfulWS;

import arduinoCom.ArduinoSerial;
import common.Constantes;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.simple.parser.JSONParser;
import org.json.simple.JSONObject;
import org.json.simple.parser.ParseException;

/**
 *
 * @author SONY
 */
public class RestfulParams {
    
    private static final int INTERVALO_PETICIONES = 250;
    
    private boolean estadoAutomatico, estadoActuador,ultimaPeticionOk;
    private Double nivelCO2, maxNivelCO2;
    private long ultimaPeticion;
    
    private static RestfulParams singleton;
    
    private RestfulParams(){
        ultimaPeticionOk = true;
        ultimaPeticion = 0;
        File ficheroJson = new File(Constantes.NOMBRE_FICHERO);
        
        if(ficheroJson.exists()){
            JSONParser parser = new JSONParser();
            JSONObject json = null;
            
            try {
                
                json = (JSONObject) parser.parse(new FileReader(ficheroJson));
                
                if(json != null){
                    Double maxNivCO2 = (Double) json.get(String.valueOf(Constantes.JSON_MAX_NIVEL_CO2));
                    maxNivelCO2 = maxNivCO2 == null ? 0.0 : maxNivCO2;
                }else{
                    maxNivelCO2 = 0.0;
                }
                
            } catch (ParseException ex) {
                Logger.getLogger(RestfulParams.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IOException ex) {
                Logger.getLogger(RestfulParams.class.getName()).log(Level.SEVERE, null, ex);
            }
            
        }else{
            maxNivelCO2 = 0.0;
        }
        
    }
    
    public void guardarEstado(){
        JSONObject json = new JSONObject();
        
        json.put(Constantes.JSON_MAX_NIVEL_CO2, maxNivelCO2);
        
        try (FileWriter file = new FileWriter(Constantes.NOMBRE_FICHERO)) {

            file.write(json.toJSONString());
            file.flush();
            file.close();
            
        } catch (IOException e) {
            Logger.getLogger(RestfulParams.class.getName()).log(Level.SEVERE, null, e);
        }
    }
    
    public static synchronized RestfulParams getRestfulParams(){
        if(singleton == null){
            singleton = new RestfulParams();
        }
        
        return singleton;
    }

    public synchronized  boolean isEstadoAutomatico() {
        return estadoAutomatico;
    }

    public synchronized boolean isEstadoActuador() {
        return estadoActuador;
    }

    public synchronized Double getNivelCO2() {
        
        if(ultimaPeticionOk && System.currentTimeMillis()-ultimaPeticion > INTERVALO_PETICIONES){
            consultarCO2();
            ultimaPeticion = System.currentTimeMillis();
        }
            
        return ultimaPeticionOk ? nivelCO2 : null;
    }

    public synchronized Double getMaxNivelCO2() {
        if(ultimaPeticionOk && System.currentTimeMillis()-ultimaPeticion > INTERVALO_PETICIONES){
            maxNivelCO2 = comprobarNivelMaxCO2();
            ultimaPeticion = System.currentTimeMillis();
        }
        return ultimaPeticionOk ? maxNivelCO2 : null;
    }
    
    private synchronized void consultarCO2() {
            try{
                nivelCO2 = Double.parseDouble(ArduinoSerial.getNivelCO2Actual());
                ultimaPeticionOk = true;
            }catch (NumberFormatException ex){
                Logger.getLogger(RestfulParams.class.getName()).log(Level.SEVERE, "ERROR: CONSULTAR CO2 - DEVUELTO VALOR NO NUMÉRICO", ex);
                nivelCO2 = null;
                ultimaPeticionOk = false;
            }
    }

    public synchronized void consultarEstado() {
        
        if(ultimaPeticionOk && System.currentTimeMillis()-ultimaPeticion > INTERVALO_PETICIONES){
            String strAux = ArduinoSerial.getEstadoActual();
            if(strAux == null){
                ultimaPeticionOk = false;
            }else{
                ultimaPeticionOk = true;
            }
            interpretarEstados(strAux);
            ultimaPeticion = System.currentTimeMillis();
            
        }
        
    }
    
    private void interpretarEstados(String estadoBruto){
        if(estadoBruto != null){
            String[] estadoAutomaticoActuador = estadoBruto.split("[/\r\n]");

            estadoAutomatico = Boolean.parseBoolean(estadoAutomaticoActuador[0]);
            estadoActuador = Boolean.parseBoolean(estadoAutomaticoActuador[1]);
            
        }
    }

    public synchronized void activarAutomatico(){
        String resAux = ArduinoSerial.encenderAutomático();
        if(resAux == null){
            ultimaPeticionOk = false;
        }else{
            ultimaPeticionOk = true;
        }
        interpretarEstados(resAux);
    }
    
    public synchronized void desactivarAutomatico(){
        String resAux = ArduinoSerial.apagarAutomático();
        if(resAux == null){
            ultimaPeticionOk = false;
        }else{
            ultimaPeticionOk = true;
        }
        interpretarEstados(resAux);
    }
    
    public synchronized void activarActuador(){
        String resAux = ArduinoSerial.abrirActuador();
        if(resAux == null){
            ultimaPeticionOk = false;
        }else{
            ultimaPeticionOk = true;
        }
        interpretarEstados(resAux);
    }
    
    public synchronized void desactivarActuador(){
        String resAux = ArduinoSerial.cerrarActuador();
        if(resAux == null){
            ultimaPeticionOk = false;
        }else{
            ultimaPeticionOk = true;
        }
        interpretarEstados(resAux);
    }
    
    public synchronized double comprobarNivelMaxCO2(){
        double result = 0.0;
        String resultAux = "-";
        try{
            resultAux = ArduinoSerial.getNivelMaximoCO2();
            result = Double.parseDouble(resultAux);
            ultimaPeticionOk = true;
        }catch (NumberFormatException ex){
            Logger.getLogger(RestfulParams.class.getName()).log(Level.SEVERE, "ERROR: CONSULTAR MAX_CO2 - DEVUELTO VALOR NO NUMÉRICO -> "+resultAux, ex);
            ultimaPeticionOk = false;
        }
        return result;
    }
    
    public synchronized void cambiarNivelMaxCO2(double nivelMaxCO2){
        String resultadoBruto = ArduinoSerial.setNivelMaximoCO2(nivelMaxCO2);
        if(resultadoBruto != null && !resultadoBruto.startsWith("ERROR:")){
            ultimaPeticionOk = true;
            maxNivelCO2 = nivelMaxCO2;
            System.out.println("Establecido maxNivelCO2: "+maxNivelCO2);
            guardarEstado();
        }else{
            System.out.println(null == resultadoBruto ? "ERROR: Respuesta null" : resultadoBruto);
            ultimaPeticionOk = false;
        }
    }
    
    public synchronized void asegurarNivelMaxCO2(){
        try{
            if(comprobarNivelMaxCO2() != maxNivelCO2){
                cambiarNivelMaxCO2(maxNivelCO2);
                ultimaPeticionOk = true;
            }
        }catch (NumberFormatException ex){
            Logger.getLogger(RestfulParams.class.getName()).log(Level.SEVERE, "ERROR: ERROR DURANTE LA COMPROBACIÓN DEL NIVEL MAX DE CO2", ex);
            ultimaPeticionOk = false;
        }
    }
}
