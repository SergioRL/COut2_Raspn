/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package restfulWS.task;

import java.util.TimerTask;
import java.util.logging.Level;
import java.util.logging.Logger;
import restfulWS.RestfulParams;

/**
 *
 * @author SeRRi
 */
public class NivelMaxCO2Task extends TimerTask{
    
    private RestfulParams restfulParams;
    
    public NivelMaxCO2Task(){
        super();
        restfulParams = RestfulParams.getRestfulParams();
    }

    @Override
    public void run() {
        try {
            restfulParams.asegurarNivelMaxCO2();
        } catch (Exception ex) {
            Logger.getLogger(NivelMaxCO2Task.class.getName()).log(Level.SEVERE, "ERROR: ERROR DURANTE TASK ASEGURAR NIVEL CO2", ex);
        }
    }
    
}
