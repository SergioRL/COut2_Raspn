/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package restfulWS.task;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.TimerTask;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author SeRRi
 */
public class ImprimirHostTask extends TimerTask{

    String cadenaIp;
    
    public ImprimirHostTask(){
        super();
        try {
            cadenaIp = InetAddress.getLocalHost().toString();
        } catch (UnknownHostException ex) {
            Logger.getLogger(ImprimirHostTask.class.getName()).log(Level.SEVERE, "ERROR : NO SE HA PODIDO RECUPERAR EL LOCALHOST", ex);
        }
    }
    
    public ImprimirHostTask(String ip, String puerto, String contexto){
        super();
        try {
            cadenaIp = "";
            cadenaIp += ip == null ? "" : ip;
            cadenaIp += puerto == null ? "" : ":"+puerto;
            cadenaIp += contexto == null ? "" : "/"+contexto;
        } catch (Exception ex) {
            Logger.getLogger(ImprimirHostTask.class.getName()).log(Level.SEVERE, "ERROR : NO SE HA PODIDO RECUPERAR EL LOCALHOST", ex);
        }
    }
    
    @Override
    public void run() {
        cadenaIp = "";
        try {
             
            Runtime rt = Runtime.getRuntime();
            String[] commands = {"/bin/bash","-c","hostname -I","|","awk,","'{print $1}'"};
            Process proc = new ProcessBuilder(commands).start();

            BufferedReader stdInput = new BufferedReader(new
                     InputStreamReader(proc.getInputStream()));

            BufferedReader stdError = new BufferedReader(new
                     InputStreamReader(proc.getErrorStream()));

            String s = null;
            while ((s = stdInput.readLine()) != null) {
                cadenaIp += s;
            }


        } catch (IOException ex) {
            cadenaIp = "error";
            Logger.getLogger(ImprimirHostTask.class.getName()).log(Level.SEVERE, null, ex);
        }
            
        
        
        if(cadenaIp != null){
            System.out.println("Informacion del host:\n"+cadenaIp);
        }
    }
    
}
