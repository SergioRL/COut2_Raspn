/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package restfulWS;

import common.Constantes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import org.json.simple.JSONObject;

/**
 *
 * @author SONY
 */

@Path("controles")
public class Controles {
    
    @Path(Constantes.GET_NIVEL_CO2_ACTUAL)
    @GET
    @Produces("application/json")
    public String getMuestra(){
        RestfulParams restfulParams = RestfulParams.getRestfulParams();
        
        String result = "NOK";
        Double muestra = restfulParams.getNivelCO2();
        
        
        if(muestra != null){

            JSONObject obj = new JSONObject();
            
            obj.put(Constantes.JSON_MUESTRA_NIVEL_CO2, muestra);
            
            result = obj.toJSONString();
        }
        
        return result;
    }
    
    @Path(Constantes.GET_ESTADO_SISTEMA)
    @GET
    @Produces("application/json")
    public String getEstadoActual(){
        
        RestfulParams restfulParams = RestfulParams.getRestfulParams();
        
        return estadoSistema(restfulParams);
    }
    
    @Path(Constantes.POST_SET_NIVEL_MAX_CO2+"/{nivelMax}")
    @POST
    @Produces("application/json")
    public String setNivelMax(@PathParam("nivelMax")Double nivelMax){
        
        RestfulParams restfulParams = RestfulParams.getRestfulParams();
        
        restfulParams.cambiarNivelMaxCO2(nivelMax);
        
        return estadoSistema(restfulParams);
    }
    
    @Path(Constantes.GET_GET_NIVEL_MAX_CO2)
    @GET
    @Produces("application/json")
    public String getNivelMax(){
        String result = null;
        
        RestfulParams restfulParams = RestfulParams.getRestfulParams();
        
        JSONObject obj = new JSONObject();
            
        obj.put(Constantes.JSON_MAX_NIVEL_CO2, restfulParams.getMaxNivelCO2());

        result = obj.toJSONString();

        return result;
    }
    
    @Path(Constantes.GET_ENCENDER_AUTOMATICO)
    @GET
    @Produces("application/json")
    public String activarAutomatico(){
        
        RestfulParams restfulParams = RestfulParams.getRestfulParams();
        
        restfulParams.activarAutomatico();
        
        return estadoSistema(restfulParams);
    }
    
    @Path(Constantes.GET_APAGAR_AUTOMATICO)
    @GET
    @Produces("application/json")
    public String desactivarAutomatico(){
        
        RestfulParams restfulParams = RestfulParams.getRestfulParams();
        
        restfulParams.desactivarAutomatico();
        
        return estadoSistema(restfulParams);
    }
    
    @Path(Constantes.GET_ABRIR_ACTUADOR)
    @GET
    @Produces("application/json")
    public String activarActuador(){
        
        RestfulParams restfulParams = RestfulParams.getRestfulParams();
        
        restfulParams.activarActuador();
        
        return estadoSistema(restfulParams);
    }
    
    @Path(Constantes.GET_CERRAR_ACTUADOR)
    @GET
    @Produces("application/json")
    public String desactivarActuador(){
        
        RestfulParams restfulParams = RestfulParams.getRestfulParams();
        
        restfulParams.desactivarActuador();
        
        return estadoSistema(restfulParams);
    }
    
    private String estadoSistema(RestfulParams restfulParams){
        
        restfulParams.consultarEstado();
        
        JSONObject obj = new JSONObject();
        
        obj.put(Constantes.JSON_ESTADO_AUTOMATICO, restfulParams.isEstadoAutomatico());
        obj.put(Constantes.JSON_ESTADO_ACTUADOR, restfulParams.isEstadoActuador());
        
        return obj.toJSONString();
    }
}
